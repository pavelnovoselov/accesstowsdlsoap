﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using WebAppTestTask.Models;

namespace WebAppTestTask.Controllers
{
    public class HomeController : Controller
    {
        private const string requestUriString = @"http://isapi.icu-tech.com/icutech-test.dll/soap/IICUTech";
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public static HttpWebRequest CreateWebRequest()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(requestUriString);
            webRequest.Headers.Add(@"SOAP:Login");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "Post";
            return webRequest;
        }

        [HttpPost]
        public async Task<ActionResult<string>> Execute(string UserName, string UserPassword)
        {
            HttpWebRequest request = CreateWebRequest();
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""UTF-8""?>
<env:Envelope xmlns:env=""http://www.w3.org/2003/05/soap-envelope"" xmlns:ns1=""urn:ICUTech.Intf-IICUTech"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:enc=""http://www.w3.org/2003/05/soap-encoding""><env:Body><ns1:Login env:encodingStyle=""http://www.w3.org/2003/05/soap-encoding"">
<UserName xsi:type=""xsd:string"">" + UserName + @"</UserName>
<Password xsi:type=""xsd:string"">" + UserPassword + @"</Password>
<IPs xsi:type=""xsd:string""></IPs></ns1:Login></env:Body></env:Envelope>");

            string soapResult;

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
            using (WebResponse response = request.GetResponse())
            {
                using StreamReader rd = new StreamReader(response.GetResponseStream());
                soapResult = rd.ReadToEnd();
                //Console.WriteLine(soapResult);
            }

            var str = soapResult.Split('{');
            var result = str[1].Split('}');

            return result[0];
        }        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}