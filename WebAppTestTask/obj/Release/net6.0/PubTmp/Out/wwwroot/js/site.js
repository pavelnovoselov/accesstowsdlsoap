﻿function execute() {
    let userName = document.getElementById('user_name').value;
    let userPassword = document.getElementById('user_password').value;

    let message = document.getElementById('message_box');
    message.innerHTML = '';

    let data = {
        UserName: userName,
        UserPassword: userPassword,
    }

    $.ajax({
        url: "/Home/Execute",
        data: data,
        type: "POST"
    }).done(function (result) {
        
        if (result.substring(1, 9) == "EntityId") {
            message.style.color = 'green';
            message.innerHTML = "вход выполнен успешно  <br>" + result.replaceAll(',','<br>');
        }
        else {
            message.style.color = 'darkred';
            message.innerHTML = 'User not found';
            console.log(result);
        }
    });
}
